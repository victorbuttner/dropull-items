# Dropull Items

## Introduction
This repository contains the smart contracts and transactions that implement the core functionality of Dropull.

The smart contracts are written in Cadence a  oriented smart contract programming language designed for the Flow Blockchain.

Features :
1. Mint NFT: Mint NFT and save it into a collection storage
2. Check Contrac: Checks if the user has the contract in account
3. Setup Account: Setup user account with the NFT contract
4. List user NFTs
5. Transfer NFT 
6. Borrow NFT: Get NFT details 

## What is dropull

A blockchain-based platform where fans can buy and sell digital assets from esports teams.

Buy and collect esport teams digital assets. Share your collection with your friends. Complete achievements to get exclusive experiences rewards. 

## What is flow

Flow is a new blockchain for open worlds. Read more about it [here](https://www.onflow.org/).


## What is Cadence

Cadence is a new Resource-oriented programming language for developing smart contracts for the Flow Blockchain. Read more about it [here](https://www.docs.onflow.org/)

## Contracts

1. Dropull.cdc: This is the main Dropull smart contract that defines the core functionality of the NFT.
2. MarketPlace.cdc: Soon!


## Directory Structure

The directories here are organized into contracts, scripts, and transactions.

Contracts contain the source code for the Dropull contracts that will be deployed to Flow.

Scripts contain read-only transactions to get information about the state of someones Collection or about the state of the Dropull contract.


## Running tests
To run tests you must have node and flow cli already installed on your machine.

1. Install node packages
`yarn install`
1. Start the emulator
`yarn emulator`
2. Run tests
`yarn test`


