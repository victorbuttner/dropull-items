module.exports = {
    testEnvironment: "node",
    verbose: true,
    coveragePathIgnorePatterns: ["/node_modules/"],
    preset: 'ts-jest',
    transform: {
      '^.+\\.(ts|tsx)?$': 'ts-jest',
      "^.+\\.(js|jsx)$": "babel-jest",
    },
    transformIgnorePatterns: ["/node_modules/flow-js-testing/"]
};