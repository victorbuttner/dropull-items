// Transaction2.cdc

import Dropull from 0x02
import NonFungibleToken from 0x01

// This transaction allows the Minter account to mint an NFT
// and deposit it into its collection.

transaction(recipient: Address) {

    // local variable for storing the minter reference
    let minter: &Dropull.DropMinter

    prepare(signer: AuthAccount) {

        // borrow a reference to the NFTMinter resource in storage
        self.minter = signer.borrow<&Dropull.DropMinter>(from: /storage/DropMinter)
            ?? panic("Could not borrow a reference to the NFT minter")
    }

    execute {
        // Borrow the recipient's public NFT collection reference
        let receiver = getAccount(recipient)
            .getCapability(/public/NFTCollection)
            .borrow<&{NonFungibleToken.CollectionPublic}>()
            ?? panic("Could not get receiver reference to the NFT Collection")

        // Mint the NFT and deposit it to the recipient's collection
        self.minter.mintNFT(recipient: receiver)
    }
}
